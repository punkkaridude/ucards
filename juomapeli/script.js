$(document).ready(function () { 

    

    function quitBox(cmd)
    {
    console.log('quit!')   
    if (cmd=='quit')
    {
        open(location, '_self').close();
    }   
    return false;   
    }

    ///$("#exit").click(function(){
    ///    quitBox('quit');
    ///});

    ///$("#logo").click(function(){
    ///    quitBox('quit');
    ///});

    ///$("#PeliLoppu i").click(function(){
    ///    quitBox('quit');
    ///})

    $("#PeliLoppu p").click(function(){
        location.reload();
    })

    $("#tek1").click(function(){
        $("#infopanel").fadeOut(200);
        blurremove();
        $("#footer").css("height", "90%");
        $(this).fadeOut(500, function(){
            $(this).text("Sulje").fadeIn(500);
        });
    });

    ///Pysyvien korttien poitaminen klikkaamalla. Jos kortti on tauko näyteään tauko.
    $("div.pelaajat").on('click','.pysyvakortti img', function(e){
            if($(e.target).is("img.peukku")){
                console.log("poistetaan peukku!");
                $(e.target).remove();
            }
            if($(e.target).is("img.tauko")){
                console.log("poistetaan taukoimg ja aloitetaan tauko");
                $(e.target).remove();
                blur();
                $("#taukoPopup").fadeIn(600);
            }
            if($(e.target).is("img.saanto")){
                console.log("poistetaan saanto!");
                $(e.target).remove();
            }
            if($(e.target).is("img.huora")){
                console.log("poistetaan huora!");
                $(e.target).remove();
            }
            
    });

    //Chris Coyier COLOR LIGHTENDARKENCOLOR
    function LightenDarkenColor(col, amt) {
        var usePound = false;
        if (col[0] == "#") {
            col = col.slice(1);
            usePound = true;
        }
        var num = parseInt(col,16);
        var r = (num >> 16) + amt;
        if (r > 255) r = 255;
        else if  (r < 0) r = 0;
        var b = ((num >> 8) & 0x00FF) + amt;
        if (b > 255) b = 255;
        else if  (b < 0) b = 0;
        var g = (num & 0x0000FF) + amt;
        if (g > 255) g = 255;
        else if (g < 0) g = 0;
        return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
    }

    ///avaa tekijät sivu footerista
    var counts = 0;
    $("#footer h2#tekija").click(function(){ 
		if(counts > 0){
            $("#footer").css("height", "7%");
            $(this).fadeOut(500, function(){
                $(this).text("Tekijät").fadeIn(2000);
            });
			counts = 0;
		}
		else{
            $("#footer").css("height", "90%");
            $(this).fadeOut(500, function(){
                $(this).text("Sulje").fadeIn(500);
            });
			counts++;
		}
    });
    
    //kuution klikkiääniefekti
    var spinsound = new Audio("audio/spinsound.mp3");

    var currentClass = '';
	$(".list").click(function(){
        spinsound.play();
		var showClass = 'show-' + this.id;
		if(currentClass){
			$(".cube").removeClass(currentClass);
		}
		$(".cube").addClass(showClass);
		currentClass = showClass;
		
		if(this.id === "back"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
                $("#cpText").html("Introvideo, Blackjack, Etusivu, Tekijät-sivu");
            });
            $("#cInfo").fadeIn(500);
			$("#C2").css("opacity", "1");
			$("#C1, #C3, #C4, #C5, #C6").css("opacity", "0");
		}
		if(this.id === "right"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html('<iframe width="420" height="345" src="https://www.youtube.com/embed/X-tFrAYVA5w" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
                $("#chText").html("Vastuualue");
                $("#cpText").html("Juomapeli");
            });
            $("#cInfo").fadeIn(500);
			$("#C3").css("opacity", "1");
			$("#C1, #C2, #C4, #C5, #C6").css("opacity", "0");
		}
		if(this.id === "left"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
                $("#cpText").html("Higher or Lower");
            });
            $("#cInfo").fadeIn(500);
			$("#C4").css("opacity", "1");
			$("#C1, #C3, #C2, #C5, #C6").css("opacity", "0");
		}
		if(this.id === "top"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
                $("#cpText").html("Muistipeli");
                $("#Video").html('<iframe width="420" height="345" src="https://www.youtube.com/embed/_nsGDd1WpvU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');

            });
            $("#cInfo").fadeIn(500);
			$("#C5").css("opacity", "1");
			$("#C1, #C3, #C4, #C2, #C6").css("opacity", "0");
		}
		if(this.id === "bottom"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
                $("#cpText").html("Kolmen kortin pokeri");
            });
            $("#cInfo").fadeIn(500);
			$("#C6").css("opacity", "1");
			$("#C1, #C3, #C4, #C5, #C2").css("opacity", "0");
		}
		if(this.id === "front"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("uCards");
                $("#cpText").html("Tämä sovellus on toteutettu osana JAMKin web-projekti 1 opintojaksoa. Sovelluksen tarkoituksena on olla interaktiivisen nuorille ja etenkin opiskelijoille suunnatun korttipeliteemainen web-sovellus. Sovellus sisältää erilaisia korttipelejä, joita loppukäyttäjän on mahdollista pelata.");
            });
            $("#cInfo").fadeIn(500);
			$("#C1").css("opacity", "1");
			$("#C2, #C3, #C4, #C5, #C6").css("opacity", "0");
		}
		
    });
    
    ///ÄÄNET ALKAA
    var vol = 0.2;
    var menumusic = new Howl({
        src: ['audio/menu.mp3'],
        preload: true,
        html5: true,
        autoUnlock: true,
        loop: true,
        volume: vol,
        onplayerror: function(){
            menumusic.once('unlock', function(){
                menumusic.stop();
                menumusic.play();
            });
        }
    });
    menumusic.play();

    var loppumusa = new Howl({
        src: ['audio/Acidbrain3.wav'],
        preload: true,
        html5: true,
        autoUnlock: true,
        loop: true,
        volume: vol,
        onplayerror: function(){
            loppumusa.once('unlock', function(){
                loppumusa.stop();
                loppumusa.play();
            });
        }
    });

    var clicksound = new Howl({
        src: ['audio/dersuperanton__taking-card.wav'],
        preload: true,
        html5: true,
        autoUnlock: true,
        volume: 0.5
    });

    var slider = $("#volumeslider");
    $("#volumeslider").slider({
        min: 0,
        max: 100,
        value: 20,
        range: "min",
        slide: function(event, ui){
           
            menumusic.volume(ui.value / 100);
            console.log("volume: " + ui.value / 100);
            tempvol = menumusic.volume();
        }
    });

    $("#mute").click(function(){
        menumusic.volume(0);
       
        slider.slider({value: 0});
        tempvol = menumusic.volume();
    });

    var tempvol = menumusic.volume();
    $("#down").click(function(){
        if (menumusic.volume() > 0.0){
            ///alert("toimii");
            ///console.log(menumusic.volume());
            tempvol -= 0.05;
        }
        menumusic.volume(tempvol)  
        slider.slider({value: tempvol*100}); 
        tempvol = menumusic.volume();
    });

    $("#up").click(function(){
        if (menumusic.volume() < 1.0){
            ///alert("toimii");
            ///console.log(menumusic.volume());
            tempvol += 0.05;
        }
        menumusic.volume(tempvol);
        slider.slider({value: tempvol*100});
    });
    
    ///KORTTILUOKKA 
    class Deck {
           
        constructor(){
            this.deck = [];
            this.decknostetut = [];
            this.img = [];
            this.imgnostetut= [];
            this.teePakka();
            this.sekoitus();
            this.lisaaKuvat();
        }

        ///luodaan pakka hakemalla maa ja arvo omista taulukoistaan ja yhdistämällä nämä omaan taulukkoon.
        teePakka(){
            const maat = ['H', 'Ru', 'Ri', 'P'];
            const arvot = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'JJ','QQ','KK' ];
            
            ///console.log(maat[0])
            for(var i = 0; i < maat.length; i++){
                for(var j = 0; j < arvot.length; j++){
                    this.deck.push(`${maat[i]}${arvot[j]}`);
                    
                    ///console.log("testailu" + maat[i]);
                    ///console.log("testailu" + arvot[j]);
                }
            }
        }

        /// sekoittaa pakan hakemalla kaksi random arvoa deck-taulukosta 1000 kertaa.
        sekoitus() {
            const { deck } = this;
            for (var i = 0; i < 1000; i++)
            {
                const pakka1 = Math.floor((Math.random() * deck.length));
                const pakka2 = Math.floor((Math.random() * deck.length));
                var tmp = deck[pakka1];
    
                deck[pakka1] = deck[pakka2];
                deck[pakka2] = tmp;
            }
            console.log("SEKOITUS: " + deck)
            return this;
        }

        ///Etsii ja lisää sekoitetun pakan kortteja vastaavat kuvat (kortti.png) 
        lisaaKuvat() {
            const { img } = this;
            const { deck } = this;
            ///console.log(deck[0]);
            for (var i = 0; i < this.deck.length; i++){
                var kuva = 'images/' + deck[i] + '.png';
                this.img.push(kuva); 
            }
            console.log("KUVAT: "+ img);
            return this;
        }
    }
    ///KORTTILUOKKA LOPPUU
    
    function peliloppu(){
        setTimeout(function(){
            menumusic.stop();
            loppumusa.play();
            blur();
            $("#PeliLoppu").fadeIn(1000);
        }, 500)
    }

    ///sääntöjen määrittäminen korttia kohden.
    function NaytaSaanto() {
        var saantonumero = nostot - 1;
        var pelaajanumero = nostaja - 1;     
        if (nostot === 53){
            peliloppu();
        }
        if (deckUusi.img[saantonumero].includes("Ace") === true){
            console.log("Ässä: vesiputous");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Vesiputous").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Aloittakaa juomaan yhtäaikaa. Ensimmäisenä saa lopettaa kortin nostaja ja sitä seuraavana nostajan kellonmyötäisellä puolella oleva ja sitä rataa. Viimeisellä on tiukat paikat!").fadeIn(500);
            }); 
        }
        if (deckUusi.img[saantonumero].includes("KK") === true){
            console.log("Kuningas: kunkkukulaus");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Kunkkukulaus").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Kilistäkää juomianne yhtäaikaa! Ensina kaula, sitten pohja, sitten keskiosa ja sitten pohja lattiaan. Kilistyksen jälkeen kaikki ottavat ison huikan! Mikäli joku mokaa kilistyksen joutuu tämä juomaan kaksi rangaistus huikkaa!").fadeIn(500);
            });
        }
        if (deckUusi.img[saantonumero].includes("QQ") === true){
            console.log("Akka: huora");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Huora").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Kortin nostaja saa valita itselleen 'Huoran' eli pelaajan joka joutuu juomaan aina kun nostaja juo. Sääntö pätee niin pitkään kun 'huora' saa saman kortin ja päättää kumota sen. Seuraava nostaja voi myös liittää pelaajan ketjuun!<br><br>Raahaa lyhty haluamallesi pelaajalle!<br><br>Mikäli päätät kumota kortin raahaa lyhty jollekkin ja klikkaa lyhdyt pois.").fadeIn(500);
                $("<img class='huora' src='images/Huora.png'>").appendTo("#saanto").fadeIn(500);
                dragtheWhore();
            });
        }
        if (deckUusi.img[saantonumero].includes("JJ") === true){
            console.log("Jätkä: sääntö");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Sääntö").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Kortin nostaja saa keksiä säännön joka on voimassa niin pitkään kun peli kestää tai kunnes seuraava sääntö-kortin nostaja kumoaa sen. Kumoaminen ei ole pakollinen.<br><br>Kumotessasi säännöt klikkaa saantö-ikonit pois pelaajilta").fadeIn(500);
                $("<img class='saanto'  src='images/Saanto.png'>").appendTo("div#pelaaja" + pelaajanumero + " div.pysyvakortti").fadeIn(500);

            });
        }
        if (deckUusi.img[saantonumero].includes("10") === true){
            console.log("10: tauko");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Tauko").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Tällä kortilla pääsee tauolle! Kortin nostaja saa päättää käyttääkö kortin heti vai säästääkö sen myöhemmäksi. Lisäksi kortinhaltija saa päättää mitä tauolla saa tehdä ja ketkä taukoon osallistuu!<br><br>Kortin voi käyttää klikkaamalla oman nimen päällä näkyvää tauko-ikonia.").fadeIn(500);
                $("<img class='tauko' src='images/Tauko.png'>").appendTo("#pelaaja" + pelaajanumero + " div.pysyvakortti").fadeIn(500);

            });
        }
        if (deckUusi.img[saantonumero].includes("H9") === true || deckUusi.img[saantonumero].includes("Ru9") === true){
            console.log("Punainen 9: jaa 9");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Jaa 9").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Päätä kuka joutuu juomaan omasta juomastaan 9 huikkaa! tai jaa huikat useammalle pelaajalle.").fadeIn(500);
            });
        }
        if (deckUusi.img[saantonumero].includes("P9") === true || deckUusi.img[saantonumero].includes("Ri9") === true){
            console.log("Musta 9: juo 9");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Juo 9").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Nostaja juo 9 huikkaa! On se elämä epäreilua...").fadeIn(500);
            });
        }
        if (deckUusi.img[saantonumero].includes("8") === true){
            console.log("8: käärmesilmä");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Käärmesilmät").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                var lisaakaarmesilmat = $("<img class='kaarmesilma' src='images/Kaarmesilma.png'>").appendTo("#pelaaja" + pelaajanumero + " div.pysyvakortti").fadeIn(500);
                $(this).html("Nostaja on nyt kreikkalainen jumalatar Medusa, joka katseellaan normaalisti muuttaisi vastakatsojan kiveksi. Nyt kuitenkin pelataan juomapeliä, eikä taikuutta ole humalasta huolimatta olemassa, joten kiveksi muuttumisen sijaan katsoja joutuu juomaan AINA 2 huikkaa!<br><br>Käärmesilmän voi tarkistaa pelaajan nimen päällä näkyvästä käärme-ikonista.").fadeIn(500);
                if($("div.pysyvakortti").find(".kaarmesilma").length){
                    console.log("jollain on kaarmisilma!");
                    $(".kaarmesilma").not("#pelaaja" + pelaajanumero + " div.pysyvakortti .kaarmesilma").remove();
                }
                if($("#pelaaja" + pelaajanumero + " div.pysyvakortti").find(".kaarmesilma").length){
                    console.log("terve, mulla on kaarmesilmat");
                    lisaakaarmesilmat;
                }
                if($("div.pysyvakortti").find(".kaarmesilma").length < 1){
                   lisaakaarmesilmat;
                }
            });
        }
        if (deckUusi.img[saantonumero].includes("7") === true){
            console.log("7: kategoria");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Kategoria").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Nostaja keksii yhden kategorian, johon jokainen vuorollaan keksii asioita. Esim. pc-komponentit: emolevy...ym. <br><br>Muista kuitenkin edellistenkin keksimät sanat, sillä se, joka toistaa edellisten keksimän sanan joutuu juomaan 2 huikkaa!").fadeIn(500);
            });
        }
        if (deckUusi.img[saantonumero].includes("6") === true){
            console.log("6: kysymysmestari");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Kysymysmestari").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                var lisaakysymysmestari = $("<img class='kysymysmestari' src='images/Kysymysmestari.png'>").appendTo("#pelaaja" + pelaajanumero + " div.pysyvakortti").fadeIn(500);
                $(this).html("Tämä kortti tekee jo valmiiksi pihalla olevan pelaajan pelistä vielä haastavampaa kun se jo on, sillä tämän kortin haltijan kysymyksiin ei saa vastata ellei tahdo juoda 2 huikkaa.<br>Muistutuksena kysymysmestari-pelaajalla on hattu-ikoni nimensä päällä.").fadeIn(500);
                if($("div.pysyvakortti").find(".kysymysmestari").length){
                    console.log("jollain on kysymysmestari!");
                    $(".kysymysmestari").not("#pelaaja" + pelaajanumero + " div.pysyvakortti .kysymysmestari").remove();
                }
                if($("#pelaaja" + pelaajanumero + " div.pysyvakortti").find(".kysymysmestari").length){
                    console.log("terve, mulla on kysymysmestari");
                    lisaakysymysmestari;
                }
                if($("div.pysyvakortti").find(".kysymysmestari").length < 1){
                    lisaakysymysmestari;
                }
            });
                
        }
        if (deckUusi.img[saantonumero].includes("5") === true){
            console.log("5: peukku");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Peukku").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Tämän kortin haltijan täytyy jossain vaiheessa peliä nostaa peukalo pystyyn siten, että kaikki voivat sen nähdä. Peukalon huomanneet tekevät samoin ja viimeinen peukalon nostanut joutuu juomaan 2 huikkaa. Kortin voi säilyttää ja käyttää milloin haluaa.<br><br>Muistutuksena pelaajasta, jolla on peukku-kortti on tämän nimen päällä peukalo-ikoni.").fadeIn(1000); 
                $("<img class='peukku' src='images/Peukku.png'>").appendTo("#pelaaja" + pelaajanumero + " div.pysyvakortti").fadeIn(500);
            });
            
        }
        if (deckUusi.img[saantonumero].includes("4") === true){
            console.log("4: lyö otsaan!");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Lyö otsaan!").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Lyö itseäsi otsaan niin nopeasti kuin voit! Viimeinen juo 2 huikkaa.").fadeIn(500);
            });
        }
        if (deckUusi.img[saantonumero].includes("H3") === true || deckUusi.img[saantonumero].includes("Ru3") === true){
            console.log("Punainen 3: jaa kolme");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Jaa 3").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Kortin nostaja päättää ketkä tai kuka onnekas joutuu juomaan 3 huikkaa. Huikat voi antaa yhdelle tai jakaa useamman kesken.").fadeIn(500); 
            });
        }
        if (deckUusi.img[saantonumero].includes("P3") === true || deckUusi.img[saantonumero].includes("Ri3") === true){
            console.log("Musta 3: juo kolme");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Juo 3").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Kortin nostaja joutuu 'valitettavasti' juomaan 3 huikaa omasta juomastaan.").fadeIn(500);
            });
        }
        if (deckUusi.img[saantonumero].includes("2") === true){
            console.log("2: tarina");
            $("#saanto h1").fadeOut(500, function(){
                $(this).text("Tarina").fadeIn(500);
            });
            $("#saanto p").fadeOut(500, function() {
                $(this).html("Tämä kortti saa humalaisten kielet solmuun! Kortin nostaja aloittaa keksimällä sanan, jonka jälkeen seuraavaa jatkaa sanomalla edellisen sanan ja jatkamalla tarinaa omalla sanallaan. Tarinaa jatketaan niin pitkään kunnes joku ei pystysanomaan koko tarinaa tai sanoo sanan väärin. Mokaaja joutuu juomaan 2 huikkaa!").fadeIn(500); 
            });
        }
    }    

    ///tekee huoraikonista draggable...voi dragata muille paitsi aktiiviselle pelaajalle eli nostajalle.
    function dragtheWhore(){
       var temp = nostaja -1;
        spaceNot = false;
       $("#carddeck").off('click');
       $("#saanto img.huora").draggable();
       $("#saanto img.huora").draggable('enable');
    
        console.log("dragtheWhore: nostaja on " + temp);
       $("div.pysyvakortti").not( $("#pelaaja" + temp + " div.pysyvakortti") ).droppable({
            disabled: false,
            drop: function(event, ui){
                ui.draggable.appendTo($(this));
                $(ui.draggable).css({ "left": "0", "top": "0" });
                ui.draggable.draggable('disable');
                $(this).droppable('disable');
                $("#carddeck").on('click', carddeckClick);
                spaceNot = true;
            }
        });
    }


    ///aloita --> valitse pelaajien määrä --> nimeä pelaajat --> pelaa
    var playercounts = 0;
    
    $("#startBtn").click(function(){
        
        console.log(" Startti Toimii");
        $("#Intro").hide(500);
        ///$(".volume").css("visibility", "visible").fadeIn(1000);
        $(".players").fadeIn(1000);
        $("input#PCount").focus();
    });

    $("#CountForm").submit(function(event){
        var temp = $("#PCount").val();
        if(temp > 0){
            playercounts = temp;
            console.log(playercounts);
            console.log("Toimii");
        
            $(".players").hide(500);
            $(".NamePlayers").fadeIn(1000);
            $("input[type=text]#PlayerCount").focus();
        } 
        event.preventDefault();  
    });

    var spaceNot = false;
    var pelaajat = [];
    var Varit = [];
    $("#startGame").submit(function(event){
        
        var input = $("#PlayerCount").val();
        var input2 = $("#Pcolor").val();
        if(input <= 1){
            input.setCustomValidity('Pelaajia täytyy olla vähintään kaksi');
        }
        else{
            console.log("Submit " + playercounts);
            pelaajat.push(input);
            Varit.push(input2);
            console.log(pelaajat);
            console.log(Varit);
            $("#PlayerCount").val("");
            $("input#PlayerCount").focus();
    
            if(pelaajat.length == playercounts){
                $(".NamePlayers").hide(500);
                $(".row").fadeIn(1000);
                spaceNot = true;
                $(".pelaajat").css("display", "flex");
                for (var pelaaja = 0; pelaaja < pelaajat.length; pelaaja++){
                    $(".pelaajat").append("<div id=pelaaja" + pelaaja +" class='pelaaja'></div>");
                    $("#pelaaja" + pelaaja).append("<div class='pysyvakortti'></div>");
                    $("#pelaaja" + pelaaja).append("<p class='nimi'>" + pelaajat[pelaaja] + "</p>");
                    var bordercolor = LightenDarkenColor(Varit[pelaaja], -20);
                    $("#pelaaja" + pelaaja  + " p.nimi").css("background-color", Varit[pelaaja]);
                    $("#pelaaja" + pelaaja  + " p.nimi").css("border", "solid 3px " + bordercolor);
                    ///$("#pelaaja" + pelaaja).append("<div  id='paikka" + pelaaja + "' class='pelaajacard'></div>");
                    console.log("PELAAJA: luodaan div" + pelaaja);

                }
            }
            event.preventDefault();
        }
        event.preventDefault();
        
    });

    $("#info").click(function(){
        blur();
        $("#infopanel").fadeIn(1000);
    });

    $("#infopanel i").click(function(){
        blurremove();
        $("#infopanel").fadeOut(700);
    });

    $("#taukoPopup i").click(function(){
        blurremove();
        $("#taukoPopup").fadeOut(700);
    })

    function blur(){
        menumusic.pause();
        $(".topnav").addClass("bodyblur");
        $("#site").addClass("bodyblur");
        $("#footer").addClass("bodyblur");
    }
    
    function blurremove(){
        menumusic.play();
        $(".topnav").removeClass("bodyblur");
        $("#site").removeClass("bodyblur");
        $("#footer").removeClass("bodyblur");
    }


    ///Luodaan nimiarray divien nimistä eli pelaajien nimistä
    function teenimiArray(divs){
        for (var i = 0; i < divs.length; i++){
            pelaajatarray.push(divs[i]);
        }
    }
    ///Korttipakan klikkaaminen ensimmäistä kertaa (vasen kiinni oleva pakka)
    $("#carddeck").one('click', function(){
        teenimiArray($("div.pelaaja").toArray());
        console.log("pelaajat arrayn koko: "+ pelaajatarray.length);
    });
                   
    ///luodaan uusi pakka peliin pakkaluokasta
    const deckUusi = new Deck();
    console.log(deckUusi.deck);
    console.log(deckUusi.img);
    
    var nostaja = 0; 
    var pelaajatarray = [];
    
    ///lasketaan mukana kenen pelaajan vuoro on nostaa kortti! Eli määritetään nostaja!
    function asetaNostaja(){   
        console.log(pelaajatarray);
        var temp = nostaja;
        var tempBigger = temp + 1;
        var tempSmaller = temp - 1;
        var parray = pelaajatarray.length - 1;
        console.log("temp(nostaja): "+temp + " tempSmaller: " + tempSmaller + " tempBigger: " + tempBigger);
        
        if(temp == 0){
            $("#pelaaja" + parray +" p.nimi").css({transform: "scale(1.0)", filter: "brightness(1)"});
            $("#pelaaja" + temp+" p.nimi").css({transform: "scale(1.5)", filter: "brightness(3)"});
            console.log("1 NOSTAJA ON PELAAJA: " + temp);
            nostaja++;
        }
        if(temp > 0 && nostaja < parray){
            $("#pelaaja" + temp+" p.nimi").css({transform: "scale(1.5)", filter: "brightness(3)"}); 
            $("#pelaaja" + tempSmaller+" p.nimi").css({transform: "scale(1.0)", filter: "brightness(1)"});
            console.log("2 NOSTAJA ON PELAAJA: " + temp);
            nostaja++;
        }
        if(temp == parray){
            $("#pelaaja" + tempSmaller+" p.nimi").css({transform: "scale(1.0)", filter: "brightness(1)"});
            $("#pelaaja" + temp+" p.nimi").css({transform: "scale(1.5)", filter: "brightness(3)"});
            console.log("3 NOSTAJA ON PELAAJA: " + temp);
            nostaja++
        }
        if(temp == pelaajatarray.length){
            nostaja = 0;
            console.log("4 NOSTAJA ON PELAAJA: " + temp);
            $("#pelaaja" + parray+" p.nimi").css({transform: "scale(1.0)", filter: "brightness(1)"});
            $("#pelaaja" + nostaja+" p.nimi").css({transform: "scale(1.5)", filter: "brightness(3)"});
            nostaja++
            
        }

    }
    ///Oikealla olevan korttipakan klikkaaminen
    ///lisäksi estää kortin klikkauksen ennen kun animaatio on loppunut.
    var esto = false;
    var estoaika = 1500;
    var nostot = 0;

    $("#carddeck").click(function(){
        carddeckClick();
    });
    
    
    $(window).keypress(function(e){
        if(spaceNot === true){
            console.log("NUolekuiaplsdk");
            if(e.keyCode === 32) {
                carddeckClick();
            }
        }
        
    });
    
    


    function carddeckClick(){
        
        console.log("click toimii");
        console.log("cloneCount: " + cloneCount)
        if (esto == true) return;
        else {
            esto = true;
            setTimeout(poistaesto, estoaika);
        }
        console.log("nostot: " + (nostot + 1));
        clicksound.play();
        asetaNostaja();
        imageAnimation();
        setTimeout(function() {
            NaytaSaanto(); 
        },500);

        nostot++; 
        cloneCount++;   
    }
    ///poistetaan esto carddeckin clikkaamisesta
    function poistaesto(){
        esto = false;
    }

    ///
    if(cloneCount > 1){
        $("#card" + cloneCount).css("box-shadow", "none");
    }

    ///clone ja animoi kortin takaosa vasemmalle.
    var cloneCount = 1;
    var timer = 300;

    function imageAnimation(){ 
        $("#carddeck").clone().attr({id: 'card' + cloneCount, class: 'card'}).appendTo(".row");
        $("#card" + cloneCount).append("<div id='card'><div class='front'></div><div class='back'></div></div>");
        console.log("Ennen css setuppia: " + "#card" + cloneCount + " #card" + " .back");
        $("#card" + cloneCount + " #card" + " .back").css({"background-size": "200px 300px", "background-image": "url("+deckUusi.img[nostot]+")"});
        $("#card" + cloneCount).animate({left: "10%"}, 500, "swing", function(){
            $(".card").css({"z-index": "1", "width":"200px", "height": "300px", "-ms-transform": "translateY(-50%) scale(1.0)", "transform": "translateY(-50%) scale(1.0)"});  
            setTimeout(flipImage, timer);
        });
    }

    ///flip animaatio
    function flipImage(){
        var temp = cloneCount - 1;
        console.log("flipImage: " + "#card" + temp + " #card")
        $("#card" + temp + " #card").toggleClass("flipped");
    }
});