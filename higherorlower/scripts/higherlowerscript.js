$(document).ready(function(){
    
    class Deck {
        
        constructor(){
            this.deck = [];
            this.deckostetut = [];
            this.img = [];
            this.imgnostetut = [];
            this.teePakka();
            this.sekoitus();
            this.lisaaKuvat();
        }
        
        teePakka(){
            const maat = ['H', 'Ru', 'Ri', 'P'];
            const arvot = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'JJ', 'QQ', 'KK'];
            'Ace' == 1;
            'JJ' == 11;
            'QQ' == 12;
            'KK' == 13;
            for(var i = 0; i < maat.length; i++){
                for(var j = 0; j < arvot.length; j++){
                    this.deck.push(`${maat[i]}${arvot[j]}`);
                }
            }
        }
        
        sekoitus(){
            const {deck} = this;
            for (var i = 0; i < 1000; i++){
                const pakka1 = Math.floor((Math.random() * deck.length));
                const pakka2 = Math.floor((Math.random() * deck.length));
                var tmp = deck[pakka1];

                deck[pakka1] = deck[pakka2];
                deck[pakka2] = tmp;
            }
            return this;
        }
        
        lisaaKuvat(){
            const {img} = this;
            const {deck} = this;
            for (var i = 0; i < this.deck.length; i++){
                var kuva = 'images/' + deck[i] + '.png';
                this.img.push(kuva);
            }
            console.log("kuvat: "+img);
            return this;
        }
    }
  
    const deckUusi = new Deck();

    var cloneCount = 1;
    var timer = 300;
    var nostot = 0;
    
    


    function imageAnimation(){ 
        $("#carddeck").clone().attr({id: 'card' + cloneCount, class: 'card'}).appendTo(".row");
        $("#card" + cloneCount).append("<div id='card'><div class='front'></div><div class='back'></div></div>");
        console.log("Ennen css setuppia: " + "#card" + cloneCount + " #card" + " .back");
        $("#card" + cloneCount + " #card" + " .back").css({"background-size": "200px 300px", "background-image": "url("+deckUusi.img[nostot]+")"});
        $("#card" + cloneCount).animate({left: "15%"}, 1000, "swing", function(){
            $(".card").css({"z-index": "1", "background-size": "200px 300px", "width":"200px", "height": "300px", "transform": "scale(1,1)"});
            
            
            setTimeout(flipImage, timer);
        });
    }
    
    function flipImage(){
        var temp = cloneCount -1;
        console.log("testi"+temp);
        $("#card" + temp + " #card").toggleClass("flipped");
    }

    function cardvalue(e){
        if (deckUusi.img[e].includes("Ace") === true){
            console.log("testi");

            uusi = 1;
        }
        if (deckUusi.img[e].includes("2") === true){
            console.log("testi2");
            uusi = 2;
        }
        if (deckUusi.img[e].includes("3") === true){
            uusi = 3;
            console.log("testi3");
        }
        if (deckUusi.img[e].includes("4") === true){
            uusi = 4;
            console.log("testi4");
        }
        if (deckUusi.img[e].includes("5") === true){
            uusi = 5;
            console.log("testi5");
        }
        if (deckUusi.img[e].includes("6") === true){
            uusi = 6;
            console.log("testi6");
        }
        if (deckUusi.img[e].includes("7") === true){
            uusi = 7;
            console.log("testi7");
        }
        if (deckUusi.img[e].includes("8") === true){
            uusi = 8;
            console.log("testi8");
        }
        if (deckUusi.img[e].includes("9") === true){
            uusi = 9;
            console.log("testi9");
        }
        if (deckUusi.img[e].includes("10") === true){
            uusi = 10;
            console.log("testi10");
        }
        if (deckUusi.img[e].includes("JJ") === true){
            uusi = 11;
            console.log("testi11");
        }
        if (deckUusi.img[e].includes("QQ") === true){
            uusi = 12;
            console.log("testi12");
        }
        if (deckUusi.img[e].includes("KK") === true){
            uusi = 13;
            console.log("testi13");
        }
    }

    
    var uusi;
    var nostettu;
    var pisteet = 0;
    var esto = false;
    var estoaika = 1500;
    var nostot = 0;

    function poistaesto(){
        esto = false;
    }

    $("#same").click(function(){
        if (esto == true) return;
        else {
            esto = true;
            setTimeout(poistaesto, estoaika);
        }
            clicksound.play();
            imageAnimation();
            cardvalue(nostot);

            nostot++;
            cloneCount++;
            console.log(nostettu);
            console.log(uusi);
            
            if(nostot === 52){
                $("#end").css("visibility", "visible")
                $("#endtext").text("Sait " + pisteet + "/51 pistettä.")
            }
            if (uusi == nostettu){
                $("#text").text("Arvasit oikein!");
                pisteet++;
                $("#pisteet").html("Pisteesi: " + pisteet)
            }
            if(uusi != nostettu) {
                $("#text").text("Arvasit väärin. Yritä uudelleen.");
            }
            nostettu = uusi;
    });

    $("#high").click(function(){
        if (esto == true) return;
        else {
            esto = true;
            setTimeout(poistaesto, estoaika);
        }
            clicksound.play();
            imageAnimation();
            cardvalue(nostot);
            nostot++;
            cloneCount++;
            console.log(nostettu);
            console.log(uusi);
            if(nostot === 52){
                $("#end").css("visibility", "visible")
                $("#endtext").text("Sait " + pisteet + "/51 pistettä.")
            }
            else{
                if(uusi > nostettu){
                    console.log("testi");
                    $("#text").text("Arvasit oikein!");
                    pisteet++;
                    $("#pisteet").html("Pisteesi: " + pisteet)
                }
                if(uusi <= nostettu){
                    $("#text").text("Arvasit väärin. Yritä uudelleen.");
                }
            }
            nostettu = uusi;
    });

    $("#low").click(function(){
        if (esto == true) return;
        else {
            esto = true;
            setTimeout(poistaesto, estoaika);
        }
            clicksound.play();
            imageAnimation();
            cardvalue(nostot);
            nostot++;
            cloneCount++;
            console.log(nostettu);
            console.log(uusi);
            if(nostot === 52){
                $("#end").css("visibility", "visible")
                $("#endtext").text("Sait " + pisteet + "/51 pistettä.")
            }
            if(uusi < nostettu){
                $("#text").text("Arvasit oikein!");
                pisteet++;
                $("#pisteet").html("Pisteesi: " + pisteet)
            }
            if(uusi >= nostettu){
                $("#text").text("Arvasit väärin. Yritä uudelleen.");
            }
            nostettu = uusi;
    });

    //äänet
    var vol = 0.2;
    var menumusic = new Howl({
        src: ['../juomapeli/audio/Acidbrain2.wav'],
        preload: true,
        html5: true,
        autoUnlock: true,
        loop: true,
        volume: vol,
        onplayerror: function(){
            menumusic.once('unlock', function(){
                menumusic.stop();
                menumusic.play();
            });
        }
    });
    menumusic.play();

    var clicksound = new Howl({
        src: ['../juomapeli/audio/dersuperanton__taking-card.wav'],
        preload: true,
        html5: true,
        autoUnlock: true,
        volume: 0.5
    });

    var slider = $("#volumeslider");
    $("#volumeslider").slider({
        min: 0,
        max: 100,
        value: 20,
        range: "min",
        slide: function(event, ui){
           
            menumusic.volume(ui.value / 100);
            console.log("volume: " + ui.value / 100);
            tempvol = menumusic.volume();
        }
    });

    $("#mute").click(function(){
        menumusic.volume(0);
       
        slider.slider({value: 0});
        tempvol = menumusic.volume();
    });

    var tempvol = menumusic.volume();
    $("#down").click(function(){
        if (menumusic.volume() > 0.0){
            ///alert("toimii");
            ///console.log(menumusic.volume());
            tempvol -= 0.05;
        }
        menumusic.volume(tempvol)  
        slider.slider({value: tempvol*100}); 
        tempvol = menumusic.volume();
    });

    $("#up").click(function(){
        if (menumusic.volume() < 1.0){
            ///alert("toimii");
            ///console.log(menumusic.volume());
            tempvol += 0.05;
        }
        menumusic.volume(tempvol);
        slider.slider({value: tempvol*100});
    });
});

//kuution kääntymisääniefekti
var spinsound = new Audio("audio/spinsound.mp3");