//korttien (cards) määrittely pelikorteille
const cards = document.querySelectorAll('.memory-card');


//korttien sekoitus pelin alkaessa
shuffle();

//---------------------------Ääniefektit---------------------------
//kortin flippimuuttuja
var hasFlippedCard = false;
//korttien lukitusmuuttuja "vapaa"
var lockBoard = false;
//Ekan ja tokan klikin kortin määritys
var firstCard, secondCard;

/*flip efektiä muokattu https://twistedwave.com/online*/
var flipsnd = new Audio("sounds/cardflip.mp3");
flipsnd.volume = 0.3;

/*Pari efekti: efektiin liitetty eteen tyhjää ääniraitaa "delay"*/
var pairsnd = new Audio("sounds/correct3.mp3");
pairsnd.volume = 0.3;

/*Kun ei oikea pari ääniefekti*/
var notpairsnd = new Audio("sounds/error.wav");
notpairsnd.volume = 0.2;

/*Peli-ohjeet ääniefekti (muokattu tyhjää ääniraitaa eteen*/
var typesnd = new Audio("sounds/typingsnd (3) (2).mp3");

/*Pelin aloitus ääniefekti*/
var startsnd = new Audio("sounds/shuffle.mp3");
startsnd.volume = 0.6;

/*Aloita uudelleen ääniefekti*/
var shuffle2 = new Audio("sounds/shuffle2.mp3")
shuffle2.volume = 0.6;


/*Ääniefekti voittaessa pelin*/
var victorysnd = new Audio("sounds/victory2.wav");
victorysnd.volume = 0.1;

//takaisin ääniefekti
var backsnd = new Audio("sounds/back (1).mp3");
backsnd.volume = 0.4

//---------------------------Ääniefektit loppuu---------------------------

//pelin aloitusfunkito:jakaa kortit, asettaa movesit, sekottaa kortit ym.
function StartGame(){
    started = false;
    setTimeout(() => {shuffle();},800);
    moves=31;
	totalmatch=0;
    resetBoard();
	unflipAll();
	moveCounter();
    clearInterval(ajastin);
    countdown = 100;
    $("#countdown").text(countdown);
	document.getElementById("game-over-text").classList.add("hidden");
	document.getElementById("victory-text").classList.add("hidden");
};

//peli loppui teksti ja paluu pelivalikkoon
$("#game-over-text").click(function(){   
	backsnd.play();
	$(this).slideUp(600);
	setTimeout(() => {
	 window.location = "index.html"; 
	},620);
	$("#memory-game").slideUp(600);
	$(".game-info-container").slideUp(600);
});

//valikon piilottaminen pelin aloittaessa
$("#startgame").click(function() {
    $("#gamemenu").slideUp(600);
    $("#memory-game").css({"display": "flex"});
    $(".game-info").slideToggle(1100);
    $("#restart").slideToggle(1100);
    $("#backtomenu").css({"visibility": "visible"});
    startsnd.play();
  	StartGame();
  });
    
  
//Taustamusiikki---------------------------
var vol = 0.1;
var menumusic = new Howl({
    src: ['sounds/Acidbrain3.wav'],
    preload: true,
    html5: true,
    autoUnlock: true,
    loop: true,
    volume: vol,
    onplayerror: function(){
        menumusic.once('unlock', function(){
            menumusic.stop();
            menumusic.play();
        });
    }
});

//pelimusiikki
menumusic.play();
var clicksound = new Howl({
    src: ['audio/dersuperanton__taking-card.wav'],
    preload: true,
    html5: true,
    autoUnlock: true,
    volume: 0.5
});

//volumeslider
var slider = $("#volumeslider");
$("#volumeslider").slider({
    min: 0,
    max: 100,
    value: 20,
    range: "min",
    slide: function(event, ui){
       
        menumusic.volume(ui.value / 100);
        console.log("volume: " + ui.value / 100);
        tempvol = menumusic.volume();
    }
});

//mutenappi
$("#mute").click(function(){
    menumusic.volume(0);
   
    slider.slider({value: 0});
    tempvol = menumusic.volume();
});

//volume alas nappi
var tempvol = menumusic.volume();
$("#down").click(function(){
    if (menumusic.volume() > 0.0){
        ///alert("toimii");
        ///console.log(menumusic.volume());
        tempvol -= 0.05;
    }
    menumusic.volume(tempvol)  
    slider.slider({value: tempvol*100}); 
    tempvol = menumusic.volume();
});

//volume ylös nappi
$("#up").click(function(){
    if (menumusic.volume() < 1.0){
        ///alert("toimii");
        ///console.log(menumusic.volume());
        tempvol += 0.05;
    }
    menumusic.volume(tempvol);
    slider.slider({value: tempvol*100});
});

//Klikkauslaskuri
 var moves;
 function moveCounter(){
     moves--;
     document.getElementById("flips").innerHTML=moves;
        if(this.moves == 0){
            clearInterval(ajastin);
            document.getElementById("game-over-text").classList.add("visible");
        }
 };

//timer muuttuja
var countdown;
//ajastin
var ajastin;
//ajastin - "aikaa jäljellä"
function timer(){
    ajastin = setTimeout("timer()", 1000);
    countdown--;
    $("#countdown").text(countdown);
	if(this.countdown == 0){
		clearInterval(ajastin);
		document.getElementById("game-over-text").classList.add("visible");
	}
};

//pelin aloitus muuttuja
var started;
//kortin flippaus klikattaessa
function flipCard() {
    if (started==false){
        started = true;
        timer();
    }
    if (lockBoard) return;
    if(this === firstCard) return;

    this.classList.add('flip');
    
    if (!hasFlippedCard){
        hasFlippedCard = true;
        firstCard = this;
        
        return;
    }
    secondCard = this; 
    lockBoard = true;
    checkForMatch();
}
  
//pareja alussa 0
var totalmatch = 0;

//funktio joka tarkistaa ovatko kortit pari
function checkForMatch(){
    
    var isMatch = firstCard.dataset.name === 
    secondCard.dataset.name;
    
    isMatch ? disableCards() : unflipCards();
		
    if(isMatch){
        pairsnd.play();
        totalmatch++;  
    }

	if (totalmatch == 6){
		clearInterval(ajastin);
        $("#victory-text").toggleClass("visible");
		victorysnd.play();
		$(".overlay-stats-time").text("100" - countdown);
		$(".overlay-stats-moves").text("31" - moves);
		
	};
    moveCounter();
};

//jos oikea korttipari löytyy, poistetaan klikkauksen eventlisteneri
function disableCards(){

    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);
    moveCounter();
    resetBoard();
};

//korttien kääntö ja ääniefekti, jos ei oikea pari
function unflipCards(){
   
     setTimeout(()=>{
		notpairsnd.play();
    }, 800);
    
    setTimeout(()=>{
        firstCard.classList.remove('flip');
        secondCard.classList.remove('flip');
        resetBoard();

    }, 1200);
};

//kaikkienkorttien unflip
function unflipAll(){
    cards.forEach(card => {
        card.addEventListener('click', flipCard);
        card.classList.remove('flip', 1000);
    });
};

//kolmannen kortin valitsemisen estäminen
function resetBoard(){
    [hasFlippedCard, lockBoard] = [false, false];
    [firstCard, secondCard] = [null, null];
}

//korttien sekoittaminen
 function shuffle() {
   cards.forEach(card => {
     var ramdomPos = Math.floor(Math.random() * 12);
     card.style.order = ramdomPos;
   });
 };

//pelilaudan restarttaaminen/korttien shuffle
$("#restart").click(function(){
	totalmatch = 0;
	setTimeout(() => {
	$("#memory-game").slideUp(500);
    $("#memory-game").slideDown(500);
	},800);
	shuffle2.play();
	StartGame();
});

//peli loppui teksti ja paluu pelivalikkoon
$("#victory-text").click(function(){
	backsnd.play();
	$(this).slideUp(600);
	setTimeout(() => {
	 window.location = "index.html"; 
	},480);
	$("#memory-game").slideUp(600);
	$(".game-info-container").slideUp(600);
});

//takaisin painikkeen toiminnot pelistä
$("#backtomenu").click(function(){
    clearInterval(ajastin);
	unflipAll();
    
	$("#memory-game").slideUp(600);
	$(".game-info").slideUp(600);
	$("#restart").slideUp(600);
	$("#gamemenu").animate({
    height: 'toggle'});
    $("#backtomenu").css({"visibility": "hidden"});
	backsnd.play();
	window.location.href = "index.html"(600);
});

//Flippaus ääniefekti klikatessa korttia
$(".memory-card").click(function(){
	flipsnd.play();    
});

//infovalikko, peliohjeet ja ääniefekti
$("#gameinfo").click(function(){
	$("#startgame").slideUp(400);
	$("#gameinfo").slideUp(400);
	$("#exitgame").slideUp(400);
    $("#info-panel").show(1500);
    
    $("#backtomenu2").css({"visibility": "visible"});
	typesnd.play();
});

//takaisin-näppäin click funktio
$("#backtomenu2").click(function(){
    
    $("#startgame").show();
	$("#gameinfo").show();
    $("#exitgame").show();
    $("#info-panel").hide();

    $("#backtomenu2").css({"visibility": "hidden"});
	backsnd.play();
});

//click-event listener korteille
cards.forEach(card => card.addEventListener('click', flipCard));