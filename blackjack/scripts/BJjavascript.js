
var kortit = []; //luodaan kortit objekti jonne generoidaan kaikki kortit
var img = []; //luodaan taulukko jonka avulla pystytään hakemaan korttia vastaava kuva

var Ppisteet = 0;
var Cpisteet = 0;
var panos = 0;
var rahat = 500;

function init(){
	
		Ppisteet = 0;
		Cpisteet = 0;
	
	var pcontent = document.getElementById("playerarea"); //tyhjennetään pelialue
	pcontent.innerHTML = " ";
	var pelaa = document.getElementById("pelaa"); //buttonin funktion määrittely
	pelaa.addEventListener('click', peli);
	
	var dCard = document.getElementById("drawCard"); //buttonin funktion määrittely
	dCard.addEventListener('click', drawCard);
	
	var addPanos = document.getElementById("panosBig");
	addPanos.addEventListener('click', panosPlus);
	
	var remPanos = document.getElementById("panosSmall");
	remPanos.addEventListener('click', panosMinus);
	
	var cardsdeck = document.getElementById("cardsdeck");
	cardsdeck.addEventListener('mousedown', deckMouseDown); //lisätään event listener
	cardsdeck.addEventListener('mouseup', deckMouseUp);
	
	//cardsdeck.addEventListener('mouseleave', deckMouseLeave); //lisätään event listener
	
	if(panos > rahat){
		document.getElementById("panosSumma").innerHTML =  rahat.toFixed(2);
	}
	
	
	var num = 0;
	kortit.splice(0,kortit.length);
	img.splice(0,img.length);// tyhjennetään taulukot
	document.getElementById("cpupoints").innerHTML = Cpisteet; 
	document.getElementById("playerpoints").innerHTML = Ppisteet; //pisteet nollille
	
	document.getElementById("panosSumma").innerHTML = panos.toFixed(2);
	document.getElementById("rahat").innerHTML = rahat.toFixed(2);
	
	
	
	for(var j = 1; j < 5; j++){
		for(var i = 1; i < 14; i++){
			
			var kortti = {maa: j, arvo:i};
			kortit.push(kortti);
			console.log("test" + kortit[num].arvo);
			console.log("length" + kortit.length);
			num++;
			
		}
	}//luodaan kortit
	
	
	
	var content = document.getElementById("cpuarea"); //jakajan korttien logiikka
	var cpucard = document.createElement("div");
	
	var test = true;
    var count = 0;
	var piste = 0;
	var card;
	content.innerHTML = " ";
	while (test)
	{
		if(piste >= 17){
			test = false;
		}
			card = turnCard("cpu");
			//var number = findImage(card);
				 if(card === kortit.length){
				card--;
				}
				var points;
				var maa;
				 var number = kortit[card].arvo;
				 if(kortit[card].maa == 1){
					 maa = "images/H"
				 }
				  if(kortit[card].maa == 2){
					 maa = "images/P"
				 }
				  if(kortit[card].maa == 3){
					 maa = "images/Ri"
				 }
				  if(kortit[card].maa == 4){
					 maa = "images/Ru"
				 }
				 points = number;
				 if(number == 11){
					 number = "JJ";
					 points = 10;
				 }
				  if(number == 12){
					 number = "QQ";
					 points = 10;
				 }
				  if(number == 13){
					 number = "KK";
					 points = 10;
				 }
				  if(number == 1){
					 number = "Ace";
					 points = 11;
				 }
				//points = valueTest(img[number].value);
				content.appendChild(cpucard);
				var cpucard = document.createElement("div");
				cpucard.setAttribute("id", "cpucard"+count);
				cpucard.setAttribute("class", "cpu");
				cpucard.style.zIndex = "0";
				
			img[count] = {state: maa, value: number}; // lisätään kortti nostettuihin.
			piste += points;
			count++;
			kortit.splice(card, 1);
	}
	console.log("Pisteet " + piste);
	var pCardCount = 0;
	while(pCardCount <= 5){
		/*
		var pArea = document.getElementById("playerarea"); //jakajan korttien logiikka
		var pCard = document.createElement("div");
		pArea.appendChild(pCard);
	    pCard.setAttribute("id", "pCard"+pCardCount);
		pCard.style.zIndex = "0";
		pCard.setAttribute("class", "cpucardback");*/
		
	var flip = document.createElement("div");
	var flipInner = document.createElement("div");
	var flipFront = document.createElement("div");
	var flipBack = document.createElement("div");
	var content = document.getElementById("playerarea");
	var topcard = document.createElement("div");
	var backcard = document.createElement("div");
	content.appendChild(flip);
	flip.appendChild(flipInner);
	flip.setAttribute("class", "flip");
	flipInner.setAttribute("class", "flipInner");
	flipInner.appendChild(flipFront);
	flipInner.appendChild(flipBack);
	flipFront.setAttribute("class", "flipFront");
	flipBack.setAttribute("class", "flipBack");
	flipFront.appendChild(topcard);
	flipBack.appendChild(backcard);
	
	flip.setAttribute("id", "pCard"+pCardCount);
	flip.style.zIndex = "0";
	flip.classList.add("cpucardback");
	topcard.classList.add("cardback");
	
		pCardCount++;
	}
	
}

function deckMouseLeave(){
	var card = document.getElementById("topcard");
	if(card == null){
		var card = document.getElementById("hover");
	}
}

function panosPlus(){
	if(Ppisteet  === 0 && Cpisteet === 0){
		panos = (panos + 1)*1.1;
		if (panos > rahat)
		{
			panos = rahat;
		}
		document.getElementById("panosSumma").innerHTML = panos.toFixed(2);
	}
	else{
		document.getElementById("relText").innerHTML = "Panosta ei voi muuttaa jos kortteja on jo nostettu ja käännetty";
	}
}

function panosMinus(){
	if(Ppisteet  === 0 && Cpisteet === 0){
		panos = (panos - 1)*0.9;
		if (panos < 0)
		{
			panos = 0;
		}
		document.getElementById("panosSumma").innerHTML = panos.toFixed(2);
	}
	else{
		document.getElementById("relText").innerHTML = "Panosta ei voi muuttaa jos kortteja on jo nostettu ja käännetty";
	}
}


// kortin nosto ja veto hiirellä

//https://www.w3schools.com/howto/howto_css_flip_card.asp

function deckMouseDown(){
	var flip = document.createElement("div");
	var flipInner = document.createElement("div");
	var flipFront = document.createElement("div");
	var flipBack = document.createElement("div");
	var content = document.getElementById("playerarea");
	var topcard = document.createElement("div");
	var backcard = document.createElement("div");
	this.style.display = "block";
	content.appendChild(flip);
	flip.appendChild(flipInner);
	flip.setAttribute("class", "flip");
	flipInner.setAttribute("class", "flipInner");
	flipInner.appendChild(flipFront);
	flipInner.appendChild(flipBack);
	flipFront.setAttribute("class", "flipFront");
	flipBack.setAttribute("class", "flipBack");
	flipFront.appendChild(topcard);
	flipBack.appendChild(backcard);
	/*
	topcard.setAttribute("id", "topcard");
	topcard.style.zIndex = "0";
	topcard.addEventListener('mouseup', deckMouseUp);
	topcard.removeEventListener('mousedown', deckMouseDown);
	topcard.addEventListener('mousedown', mouseDown);
	topcard.addEventListener('click', turnCard);
	topcard.addEventListener('mouseup', mouseUp);
	*/
	flip.setAttribute("id", "topcard");
	flip.style.zIndex = "0";
	flip.addEventListener('mouseup', deckMouseUp);
	flip.removeEventListener('mousedown', deckMouseDown);
	flip.addEventListener('mousedown', mouseDown);
	flip.addEventListener('click', turnCard);
	flip.addEventListener('mouseup', mouseUp);
	window.addEventListener('mousemove', deckMoveDiv);
	console.log("MOUSEDOWNN!!!!!!!");
}

function deckMouseUp() {
	console.log("DECKMOUSEUP!!!!!!!");
	var topcard = document.getElementById("topcard");
	topcard.removeEventListener('mouseup', deckMouseUp);
	window.removeEventListener('mousemove', deckMoveDiv);
	topcard.children[0].children[0].children[0].classList.add("cardback");
	topcard.removeAttribute("id", "topcard");
}

function mouseUp(e) {
	console.log("NORMIMOUSEUP!!!!!!!");
	console.log("Viimenen" +this.id);
	var deck = document.getElementById("cardsdeck");
	deck.style.display = "block";
	this.style.zIndex = '100';
	this.removeAttribute("id", "hover");
	window.removeEventListener('mousemove', moveDiv);
	
}

function mouseDown() {
	console.log("NORMIMOUSEDOWN!!!!!!!");
	this.setAttribute( "id", "hover");
	this.style.zIndex = '101';
	window.addEventListener('mousemove', moveDiv);
	console.log("Alku" +this.id);
}

//kortin kääntäjän funktio joka tarkistaa onko kääntäjä jakaja vai pelaaja

var spinsound = new Audio("audio/dersuperanton__taking-card.wav");

function turnCard(e){
	spinsound.play();
	var cpu = "cpu";
	var player = "player";
	if(e != cpu){
			if(this.classList.contains("cpucardback")){
				this.classList.remove("cpucardback");
			}
			if(this.classList.contains("turned")){
				}
			else{
			this.id = "turned"+kortit.length;
			console.log(cpu);
			console.log("aaaaaaaaa"+this.id);
			cardTest(this.id);
			
			
				}
			}
	else{
	
	return cardTest(cpu);
	
	}

}

//kortin nostologiikka randomilla

function cardTest(e){
	console.log(this.id);
		 var points;
				 var rnd = Math.floor(Math.random() * kortit.length + 1) ;
				 var number = findImage(rnd);
				 
				 if( e == "cpu")
					{
					 return rnd;
					 }
				 else
					{
					document.getElementById("panosBig").classList.add("disabled");
					document.getElementById("panosSmall").classList.add("disabled");
					points = valueTest(img[number].value);
					 var point = Ppisteet;
					 point += points;
					 Ppisteet = point;
					 document.getElementById("playerpoints").innerHTML = Ppisteet;
					 console.log("Rng " +e);
					 document.getElementById(e).children[0].children[1].children[0].classList.add("cardfront");
					 document.getElementById(e).children[0].children[1].children[0].style.backgroundImage = "url('"+img[number].state+img[number].value+".png')";
					kortit.splice(rnd, 1);
					//document.getElementById(e).children[0].classList.add("turned");
					document.getElementById(e).classList.add("turned");
					}
}


// etsii kortille sitä vastaavan kuvan

function findImage(rnd){
	var maa;
	var count = img.length + 1;
	 if(rnd === kortit.length){
		rnd--;
		}
	 console.log("Rngg " +kortit.length);
	  console.log("Rng " +kortit[rnd].maa);
	 var number = kortit[rnd].arvo;
	 if(kortit[rnd].maa == 1){
		 maa = "images/H"
	 }
	  if(kortit[rnd].maa == 2){
		 maa = "images/P"
	 }
	  if(kortit[rnd].maa == 3){
		 maa = "images/Ri"
	 }
	  if(kortit[rnd].maa == 4){
		 maa = "images/Ru"
	 }
	 points = number;
	 if(number == 11){
		 number = "JJ";
		 points = 10;
	 }
	  if(number == 12){
		 number = "QQ";
		 points = 10;
	 }
	  if(number == 13){
		 number = "KK";
		 points = 10;
	 }
	  if(number == 1){
		 number = "Ace";
		 points = 11;
	 }
	 img[count] = {state: maa, value: number};
	 return count;
	
}

//hakee kortin kuvan arvoa vastaan sille pistearvon

function valueTest(e){
	var number = e;
	var points;
	
	 points = number;
	
	if(number == "JJ"){
		 number = "JJ";
		 points = 10;
	 }
	  if(number == "QQ"){
		 number = "QQ";
		 points = 10;
	 }
	  if(number == "KK"){
		 number = "KK";
		 points = 10;
	 }
	  if(number == "Ace"){
		 number = "Ace";
		 points = 11;
	 }
	return points;
}

function drawCard(){
	if(Cpisteet === 0){
	spinsound.play();
	var margin = 20;
	var count = 0;
	while(count < 6){
		var card = document.getElementById("pCard"+count);
		var test =  kortit.length+1;//ksssssssssssssssssssssssssssssss
		console.log(test);
		if (card === null){
			var card1 = document.getElementById("turned"+test);
				if (card1 === null){
					break;
				}
				else{
					count++;
					margin = margin+9;
				}
			
			
		}
		else if (card.style.marginLeft === margin+"%"){
			count++;
			margin = margin+9;
			continue;
		}
		else if (card.style.marginLeft != margin+"%"){
		card.style.zIndex = "100";
		card.style.marginLeft = margin+"%";
		card.style.marginTop = "35%";
		card.style.position = "absolute";
		card.addEventListener('click', turnCard);
		console.log("toimiiko");
		break;
		}
	}
}
	else{
		document.getElementById("relText").innerHTML = "Kortteja ei voi nostaa kun jakajan käsi on näkyvissä";
	}
	
}


//buttonin funktio joka tuo esiin jakajan kortit

function peli(){
	var kortit = true;
	var count = 0;
	var margin = 25;
	var test = 0;
	var point = Cpisteet;
	if (point != 0){
		init();
		Cpisteet = 0;
		Ppisteet = 0;
		document.getElementById("drawCard").classList.remove("disabled");
		document.getElementById("panosBig").classList.remove("disabled");
		document.getElementById("panosSmall").classList.remove("disabled");
		document.getElementById("pelaa").innerHTML = "Valmis";
		document.getElementById("relText").innerHTML = "Käännä nostetut kortit klikkaamalla niitä. <br><br> Klikkaa valmis painiketta kun olet valmis. <br><br> Klikkaa jatka painiketta aloittaaksesi uuden kierroksen.";
	}
	else{
		spinsound.play();
		while (kortit){
			var cpucard = document.getElementById("cpucard"+count);
			if (cpucard != null){
				test = 0;
				cpucard.style.zIndex = "100";
				cpucard.setAttribute("class", "cpucardback");
				cpucard.style.marginLeft = margin + "%";
				cpucard.style.backgroundImage = "url('"+img[count].state+img[count].value+".png')";
				
				
				var points = valueTest(img[count].value);
				var point = Cpisteet;
				point += points;
				Cpisteet = point
				document.getElementById("cpupoints").innerHTML = Cpisteet;
				 
				 
				 
				count++;
				while(test <= 9){
					margin++;
					test++;
				}

			}
			else{
				kortit = false;
			}
		}
		calcMoney();
		document.getElementById("drawCard").classList.add("disabled");
		document.getElementById("panosBig").classList.add("disabled");
		document.getElementById("panosSmall").classList.add("disabled");
		document.getElementById("pelaa").innerHTML = "Jatka";
		
	}
}

function calcMoney(){
	var bool1 = Cpisteet >= Ppisteet && Cpisteet <= 21;
	var bool2 = Cpisteet <= Ppisteet && Cpisteet >= 21;
	var bool4 = Cpisteet <= 21 &&  Ppisteet > 21;
	var bool3 =  bool1 || bool2 || bool4;
	console.log("bool 1"+bool1);
	console.log("bool 2"+bool2);
	console.log("bool 3"+bool3);
	
	if (bool1 || bool2 || bool3 || bool4){
		rahat = rahat - panos;
	}
	else{
		rahat = rahat + panos;
	}
	document.getElementById("rahat").innerHTML = rahat.toFixed(2);
	if(panos > rahat){
		panos = rahat;
	}
	document.getElementById("panosSumma").innerHTML = panos.toFixed(2);
	
}

//draggaus funktioita

function deckMoveDiv(e){
	if(Cpisteet === 0){
	var card = document.getElementById("topcard");
	if(card == null){
		var card = document.getElementById("hover");
	}
	card.style.top = (e.clientY)  +  'px';
	card.style.left = (e.clientX)  + 'px';
	}
}

function moveDiv(e){
	console.log("Loppu" +arguments[0]);
	console.log("Loppu" +e);
	if(e.clientY != null){
	var card = document.getElementById("hover");
	card.style.top = (e.clientY)  +  'px';
	card.style.left = (e.clientX)  + 'px';
	}
}