$(document).ready(function(){

	var count = 0;
	var countTek = 0;
	var inf = 0;
	var test = 0;
	var mute = 0;

	
	
	$("#DMute").click(function(){
		if(mute === 0){	
		$("#DMute h2").html("Unmute");
		mute = 1;
		}
		else{
		$("#DMute h2").html("Mute");
		mute = 0;
		}
		
	});
	$(".showTek").click(function(){
		
		if(countTek > 0){
			$("#footer").css("height", "7%");
			countTek = 0;
			$("#openClose").css("opacity", "0");
		}
		else{
			$("#footer").css("height", "91.1%");
			countTek++;
			$("#openClose").css("opacity", "1");
		}
	});
	
	$(".men h2").click(function(){
		$("#DInfo").css("background", "transparent");
		remInfo();
		count = 0;
	});
	
	$(".showInfo").click(function(){
		
		if(count > 0){
			$("#DInfo").css("background", "transparent");
			remInfo();
			count = 0;
		}
		else{
			$("#DInfo").css("background", "white");
			showInfo();
			count++;
		}
	});
	
	function showInfo(){
		
		var infoo = document.getElementById("infoOut");
		infoo.setAttribute("id", "infoIn");
			
	}
	function remInfo(){
			var infoo = document.getElementById("infoIn");
			infoo.setAttribute("id", "infoOut");
		}
	
	var currentClass = '';
	
	//kuution kääntymisääniefekti
    var spinsound = new Audio("audio/spinsound.mp3");


	$(".list").click(function(){
		spinsound.play();

		var showClass = 'show-' + this.id;
		if(currentClass){
			$(".cube").removeClass(currentClass);
		}
		$(".cube").addClass(showClass);
		currentClass = showClass;
		
		if(this.id === "back"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
                $("#cpText").html("Introvideo, Blackjack, Etusivu, Tekijät-sivu");
            });
            $("#cInfo").fadeIn(500);
			$("#C2").css("opacity", "1");
			$("#C1, #C3, #C4, #C5, #C6").css("opacity", "0");
		}
		if(this.id === "right"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html('<iframe width="420" height="345" src="https://www.youtube.com/embed/X-tFrAYVA5w" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
                $("#chText").html("Vastuualue");
                $("#cpText").html("Juomapeli");
            });
            $("#cInfo").fadeIn(500);
			$("#C3").css("opacity", "1");
			$("#C1, #C2, #C4, #C5, #C6").css("opacity", "0");
		}
		if(this.id === "left"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
                $("#cpText").html("Higher or Lower");
            });
            $("#cInfo").fadeIn(500);
			$("#C4").css("opacity", "1");
			$("#C1, #C3, #C2, #C5, #C6").css("opacity", "0");
		}
		if(this.id === "top"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
				$("#cpText").html("Muistipeli");
				$("#Video").html('<iframe width="420" height="345" src="https://www.youtube.com/embed/_nsGDd1WpvU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');

            });
            $("#cInfo").fadeIn(500);
			$("#C5").css("opacity", "1");
			$("#C1, #C3, #C4, #C2, #C6").css("opacity", "0");
		}
		if(this.id === "bottom"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("Vastuualue");
                $("#cpText").html("Kolmen kortin pokeri");
            });
            $("#cInfo").fadeIn(500);
			$("#C6").css("opacity", "1");
			$("#C1, #C3, #C4, #C5, #C2").css("opacity", "0");
		}
		if(this.id === "front"){
            $("#cInfo").fadeOut(500, function(){
                $("#Video").html("");
                $("#chText").html("uCards");
                $("#cpText").html("Tämä sovellus on toteutettu osana JAMKin web-projekti 1 opintojaksoa. Sovelluksen tarkoituksena on olla interaktiivisen nuorille ja etenkin opiskelijoille suunnatun korttipeliteemainen web-sovellus. Sovellus sisältää erilaisia korttipelejä, joita loppukäyttäjän on mahdollista pelata.");
            });
            $("#cInfo").fadeIn(500);
			$("#C1").css("opacity", "1");
			$("#C2, #C3, #C4, #C5, #C6").css("opacity", "0");
		}
		
	});
	
	
});